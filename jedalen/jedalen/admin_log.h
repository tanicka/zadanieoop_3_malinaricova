﻿#pragma once
#include <QDialog>
#include "ui_admin_log.h"

class admin_log : public QDialog {
	Q_OBJECT

public:
	admin_log(QWidget * parent = Q_NULLPTR);
	~admin_log();

	QString nacMeno();
	QString nacHeslo();

	public slots:
		void click();

private:
	Ui::admin_log ui;
	QString meno;
	QString heslo;
};
