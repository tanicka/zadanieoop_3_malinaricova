#ifndef JEDALEN_H
#define JEDALEN_H

#include <QtWidgets/QMainWindow>
#include "ui_jedalen.h"
#include<log_in.h>
#include<qmessagebox.h>
#include<qstring.h>
#include<admin.h>
#include<qfile.h>
#include<admin_log.h>
#include<qlineedit.h>
#include<qcheckbox.h>
#include<qtextedit.h>
#include<math.h>

class jedalen : public QMainWindow
{
	Q_OBJECT

public:
	jedalen(QWidget *parent = 0);
	~jedalen();

	
	public slots:
		void prihl();
		void admn();
		void dni_menu();
		void odhl();
		void vyber();
		void vymazat();
		void dobit();
		void zisti_ci_dost_kred();

private:
	Ui::jedalenClass ui;
	bool ok;
	bool adm_ok;
	log_in login;
//	int poc = login.pocet;
	QString kredit2;
	QString meno_prihl;
	bool penaz=true;
	double cena=0;
	QString kreditString;
	double kredit;
	QList<QString> heslaa;
	QList<QString> pohlaviaa;
	QList<QString> kredityy;
	int index;
	
};

#endif // JEDALEN_H
