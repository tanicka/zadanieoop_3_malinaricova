﻿#pragma once
#include <QDialog>
#include "ui_admin.h"
#include<qfile.h>
#include<qtextstream.h>
#include<qcombobox.h>
#include<jedalen.h>
#include<qlineedit.h>



class Admin : public QDialog {
	Q_OBJECT

public:
	Admin(QWidget * parent = Q_NULLPTR);
	~Admin();

	public slots:
		void nac_pouz();
		void zapis();
		void zmazat();


private:
	Ui::admin ui;
};