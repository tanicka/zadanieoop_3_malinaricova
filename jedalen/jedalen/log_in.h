﻿#pragma once
#include <QDialog>
#include "ui_log_in.h"
#include<QComboBox>
#include<qlineedit.h>
#include<qtextstream.h>
#include<qstring.h>

class log_in : public QDialog {
	Q_OBJECT

public:
	log_in(QWidget * parent = Q_NULLPTR);
	~log_in();

	QString dajHeslo();
	QString heslo;
	int dajMeno();	
	QString dajMeno2();
	QString meno2;

	public slots:
		void prihl_click();
		void nacitaj();
	
private:
	Ui::log_in ui;
	
	int meno;
	
	QList<QString> mena;
	
};
