#include "jedalen.h"

jedalen::jedalen(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

jedalen::~jedalen()
{

}


void jedalen::dni_menu()
{
	QString line;
		
		if (ui.comboBox->currentIndex() == 1) 
		{	
			QFile pondelok("pondelokk.txt");
			if (pondelok.open(QFile::ReadOnly)) {
			QTextStream pouzivatel(&pondelok);
			while (!pouzivatel.atEnd()) {
				line = pouzivatel.readLine();
				ui.checkBox->setText(line);
				line = pouzivatel.readLine();
				ui.checkBox_2->setText(line);
				line = pouzivatel.readLine();
				ui.checkBox_3->setText(line);
				line = pouzivatel.readLine();
				ui.checkBox_4->setText(line);
				line = pouzivatel.readLine();
				ui.checkBox_5->setText(line);
				}		
			}
			pondelok.close();

			QFile pceny("pceny.txt");
			if (pceny.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&pceny);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.label_5->setText(line);
					line = pouzivatel.readLine();
					ui.label_6->setText(line);
					line = pouzivatel.readLine();
					ui.label_7->setText(line);
					line = pouzivatel.readLine();
					ui.label_8->setText(line);
					line = pouzivatel.readLine();
					ui.label_9->setText(line);
				}
			}
			pceny.close();

		}

		if (ui.comboBox->currentIndex() == 2)
		{
			QFile utorok("utorok.txt");
			if (utorok.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&utorok);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.checkBox->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_2->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_3->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_4->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_5->setText(line);
				}
			}
			utorok.close();

			QFile utceny("utceny.txt");
			if (utceny.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&utceny);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.label_5->setText(line);
					line = pouzivatel.readLine();
					ui.label_6->setText(line);
					line = pouzivatel.readLine();
					ui.label_7->setText(line);
					line = pouzivatel.readLine();
					ui.label_8->setText(line);
					line = pouzivatel.readLine();
					ui.label_9->setText(line);
				}
			}
			utceny.close();
		}

		if (ui.comboBox->currentIndex() == 3)
		{
			QFile streda("streda.txt");
			if (streda.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&streda);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.checkBox->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_2->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_3->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_4->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_5->setText(line);
				}
			}
			streda.close();

			QFile stceny("stceny.txt");
			if (stceny.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&stceny);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.label_5->setText(line);
					line = pouzivatel.readLine();
					ui.label_6->setText(line);
					line = pouzivatel.readLine();
					ui.label_7->setText(line);
					line = pouzivatel.readLine();
					ui.label_8->setText(line);
					line = pouzivatel.readLine();
					ui.label_9->setText(line);
				}
			}
			stceny.close();
		}

		if (ui.comboBox->currentIndex() == 4)
		{
			QFile stvrtok("stvrtok.txt");
			if (stvrtok.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&stvrtok);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.checkBox->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_2->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_3->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_4->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_5->setText(line);
				}
			}
			stvrtok.close();

			QFile stvceny("stvceny.txt");
			if (stvceny.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&stvceny);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.label_5->setText(line);
					line = pouzivatel.readLine();
					ui.label_6->setText(line);
					line = pouzivatel.readLine();
					ui.label_7->setText(line);
					line = pouzivatel.readLine();
					ui.label_8->setText(line);
					line = pouzivatel.readLine();
					ui.label_9->setText(line);
				}
			}
			stvceny.close();
		}

		if (ui.comboBox->currentIndex() == 5)
		{
			QFile piatok("piatok.txt");
			if (piatok.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&piatok);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.checkBox->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_2->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_3->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_4->setText(line);
					line = pouzivatel.readLine();
					ui.checkBox_5->setText(line);
				}
			}
			piatok.close();

			QFile piaceny("piaceny.txt");
			if (piaceny.open(QFile::ReadOnly)) {
				QTextStream pouzivatel(&piaceny);
				while (!pouzivatel.atEnd()) {
					line = pouzivatel.readLine();
					ui.label_5->setText(line);
					line = pouzivatel.readLine();
					ui.label_6->setText(line);
					line = pouzivatel.readLine();
					ui.label_7->setText(line);
					line = pouzivatel.readLine();
					ui.label_8->setText(line);
					line = pouzivatel.readLine();
					ui.label_9->setText(line);
				}
			}
			piaceny.close();
		}
}

void jedalen::prihl()
{
	log_in login;
	QMessageBox mbox;
	login.exec();
	
	meno_prihl = login.dajMeno2();

	QFile pasword("hesla.txt");									//DLZKA SUBORU HESIEL
	if (pasword.open(QFile::ReadOnly))
	{
		QTextStream hsl(&pasword);
		while (!hsl.atEnd()) {
			heslaa.append(hsl.readLine());
		}
		pasword.close();
	}

	QFile pohl("pohlavia.txt");								  //DLZKA SUBORU POHLAVII
	if (pohl.open(QFile::ReadOnly))
	{
		QTextStream phl(&pohl);
		while (!phl.atEnd()) {
			pohlaviaa.append(phl.readLine());
		}
		pohl.close();
	}

	QFile kkredit("kredity.txt");							//DLZKA SUBORU KREDITOV
	if (kkredit.open(QFile::ReadOnly))
	{
		QTextStream credit(&kkredit);
		while (!credit.atEnd()) {
			kredityy.append(credit.readLine());
		}
		kkredit.close();
	}

	index = login.dajMeno();


	if (login.dajHeslo() == heslaa[index]) {
		ok = true;
	}
	else ok = false;

	if (ok == true) {
		QString meno_uz = login.dajMeno2();
		ui.label_2->setText(meno_uz);

		ui.lineEdit->setText(kredityy[index]);
		ui.label_4->setText(pohlaviaa[index]);	
		kreditString = ui.lineEdit->text();
		kredit = kreditString.toDouble();
	}

	if (ok == false) {
		mbox.setText("Zle zadane heslo");
		mbox.exec();
	}

}

void jedalen::odhl()
{
	ui.label_2->setText("Uzivatel");
	ui.label_4->setText("Pohlavie");
	ui.lineEdit->setText(" ");
	ui.comboBox_2->clear();
	ui.lineEdit_2->clear();
	if (ui.checkBox->isChecked()) { ui.checkBox->setChecked(false); }
	if (ui.checkBox_2->isChecked()) { ui.checkBox_2->setChecked(false); }
	if (ui.checkBox_3->isChecked()) { ui.checkBox_3->setChecked(false); }
	if (ui.checkBox_4->isChecked()) { ui.checkBox_4->setChecked(false); }
	if (ui.checkBox_5->isChecked()) { ui.checkBox_5->setChecked(false); }
}


void jedalen::zisti_ci_dost_kred()
{						
	double cena1=0, cena2=0, cena3=0, cena4=0, cena5 = 0;
	kreditString = ui.lineEdit->text();
	kredit = kreditString.toDouble();
	penaz = true;

	if (ui.checkBox->isChecked())
	{
		QString	cenaString1 = ui.label_5->text();
		cena1 = cenaString1.toDouble();

	}
	if (ui.checkBox_2->isChecked())
	{
		QString	cenaString2 = ui.label_6->text();
		cena2 = cenaString2.toDouble();
	}
	if (ui.checkBox_3->isChecked())
	{
		QString	cenaString3 = ui.label_7->text();
		cena3 = cenaString3.toDouble();
	}
	if (ui.checkBox_4->isChecked())
	{
		QString	cenaString4 = ui.label_8->text();
		cena4 = cenaString4.toDouble();
	}
	if (ui.checkBox_5->isChecked())
	{
		QString	cenaString5 = ui.label_9->text();
		cena5 = cenaString5.toDouble();
	}
	cena = cena1 + cena2 + cena3 + cena4 + cena5;

	if (cena > kredit) { 
		QMessageBox mbox;
		mbox.setText("Mate malo kreditu."); 
		mbox.exec(); 
		ui.checkBox->setChecked(false);
		ui.checkBox_2->setChecked(false);
		ui.checkBox_3->setChecked(false);
		ui.checkBox_4->setChecked(false);
		ui.checkBox_5->setChecked(false);
		cena = 0; cena2 = 0; cena3 = 0; cena4 = 0; cena5 = 0;
		penaz = false;
	}
	
}


void jedalen::vyber()
{
	QMessageBox mbox;

	if (ok == true)
	{
		zisti_ci_dost_kred();
		if (penaz == true)
		{
			QString line;
			QFile objednane(meno_prihl + ".txt");
			if (objednane.open(QFile::ReadWrite | QFile::Text))
			{
				QTextStream obj(&objednane);
				while (!obj.atEnd()) {
					line = obj.readLine();
				}
				if (ui.checkBox->isChecked()) {
					QString jedlo;
					jedlo = ui.checkBox->text();
					ui.comboBox_2->addItem(jedlo);
					obj << jedlo << endl;
					ui.checkBox->setChecked(false);
				}

				if (ui.checkBox_2->isChecked()) {
					QString jedlo;
					jedlo = ui.checkBox_2->text();
					ui.comboBox_2->addItem(jedlo);
					obj << jedlo << endl;
					ui.checkBox_2->setChecked(false);
				}
				if (ui.checkBox_3->isChecked()) {
					QString jedlo;
					jedlo = ui.checkBox_3->text();
					ui.comboBox_2->addItem(jedlo);
					obj << jedlo << endl;
					ui.checkBox_3->setChecked(false);
				}
				if (ui.checkBox_4->isChecked()) {
					QString jedlo;
					jedlo = ui.checkBox_4->text();
					ui.comboBox_2->addItem(jedlo);
					obj << jedlo << endl;
					ui.checkBox_4->setChecked(false);
				}
				if (ui.checkBox_5->isChecked()) {
					QString jedlo;
					jedlo = ui.checkBox_5->text();
					ui.comboBox_2->addItem(jedlo);
					obj << jedlo << endl;
					ui.checkBox_5->setChecked(false);
				}
				objednane.close();
			}
			//ODCITAVANIE KREDITU
			QFile kreditt("kredity.txt");
			QString kredityAll;
			QRegularExpression pov_kr(kreditString);

			if (kreditt.open(QFile::ReadOnly | QFile::Text))
			{
				double novy_kr = kredit;
				if (cena != 0) novy_kr = kredit - cena;
				QString novy_kr_string = QString::number(novy_kr);
				kredityAll = kreditt.readAll();
				kredityAll.replace(pov_kr, novy_kr_string);
				ui.lineEdit->setText(novy_kr_string);
			}
			kreditt.close();

			QFile new_kred("kredity.txt");
			if (new_kred.open(QFile::WriteOnly | QFile::Text))
			{
				QTextStream out(&new_kred);
				out << kredityAll;
			}
			new_kred.close();
		}
	}
				
	else
	{
		if (ui.checkBox->isChecked() || ui.checkBox_2->isChecked() || ui.checkBox_3->isChecked() || ui.checkBox_4->isChecked() || ui.checkBox_5->isChecked()) {
			mbox.setText("Je nutne prihlasenie.");
			mbox.exec();
		}
	}
}

	void jedalen::vymazat()
	{
		QString vym = ui.comboBox_2->currentText();
		QString line;
		QFile objednane(meno_prihl + ".txt");
		if (objednane.open(QFile::ReadWrite | QFile::Text))
		{
			QString s;
			QTextStream t(&objednane);

			while (!t.atEnd())
			{
				QString line = t.readLine();
				if (!line.contains(vym))
					s.append(line + "\n");
			}
			objednane.resize(0);
			t << s;
			objednane.close();
		}

}

void jedalen::dobit()
{	
	kreditString = ui.lineEdit->text();
	kredit = kreditString.toDouble();
	QFile kreditt("kredity.txt");
	QString kredityAll;
	QRegularExpression pov_kr(kreditString);

	if (kreditt.open(QFile::ReadOnly | QFile::Text))
	{
		double novy_kr = kredit ;
//		ui.label_13->setText(QString::number(kredit));
		QString nabitie_str=ui.lineEdit_2->text();
		double nabitie = nabitie_str.toDouble();
		novy_kr = kredit + nabitie;
	//	ui.label_15->setText(QString::number(nabitie));
	//	ui.label_14->setText(QString::number(novy_kr));
		QString novy_kr_string = QString::number(novy_kr);
		kredityAll = kreditt.readAll();
		kredityAll.replace(pov_kr, novy_kr_string);
		ui.lineEdit->setText(novy_kr_string);
	}
	kreditt.close();

	QFile new_kredit("kredity.txt");
	if (new_kredit.open(QFile::WriteOnly | QFile::Text))
	{
		QTextStream out(&new_kredit);
		out << kredityAll;	
	}
	new_kredit.close();
}



void jedalen::admn()
{
	QMessageBox mbox;
	QString menoA = "admin";
	QString hesloA = "xadmin";

	admin_log al;
	al.exec();
	if (al.nacMeno() == menoA && al.nacHeslo() == hesloA)
	{
		Admin ad;
		ad.exec();
		adm_ok = true;
	}
	else
		mbox.setText("Zle zadane meno alebo heslo");
		mbox.exec();
}
