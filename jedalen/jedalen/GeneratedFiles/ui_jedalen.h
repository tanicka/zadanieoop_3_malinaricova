/********************************************************************************
** Form generated from reading UI file 'jedalen.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JEDALEN_H
#define UI_JEDALEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_jedalenClass
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *label_2;
    QComboBox *comboBox;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QLabel *label_4;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QLabel *label_10;
    QLabel *label_11;
    QComboBox *comboBox_2;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QLineEdit *lineEdit_2;
    QLabel *label_12;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *jedalenClass)
    {
        if (jedalenClass->objectName().isEmpty())
            jedalenClass->setObjectName(QStringLiteral("jedalenClass"));
        jedalenClass->resize(710, 754);
        centralWidget = new QWidget(jedalenClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(230, 20, 181, 51));
        QFont font;
        font.setFamily(QStringLiteral("Segoe Print"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(440, 30, 112, 34));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(30, 40, 112, 34));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 110, 191, 19));
        QFont font1;
        font1.setPointSize(12);
        label_2->setFont(font1);
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(10, 140, 121, 25));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(460, 110, 68, 19));
        label_3->setFont(font1);
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(540, 110, 141, 25));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(250, 110, 91, 19));
        label_4->setFont(font1);
        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(50, 180, 611, 23));
        checkBox_2 = new QCheckBox(centralWidget);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(50, 220, 481, 23));
        checkBox_3 = new QCheckBox(centralWidget);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(50, 260, 491, 23));
        checkBox_4 = new QCheckBox(centralWidget);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(50, 300, 491, 23));
        checkBox_5 = new QCheckBox(centralWidget);
        checkBox_5->setObjectName(QStringLiteral("checkBox_5"));
        checkBox_5->setGeometry(QRect(50, 340, 481, 23));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(580, 180, 68, 19));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(580, 220, 68, 19));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(580, 260, 68, 19));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(580, 300, 68, 19));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(580, 340, 68, 19));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(570, 30, 112, 34));
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(300, 390, 112, 34));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(30, 440, 121, 19));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(580, 150, 68, 19));
        comboBox_2 = new QComboBox(centralWidget);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(30, 480, 401, 25));
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(470, 480, 112, 34));
        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(20, 560, 112, 34));
        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(160, 570, 113, 25));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(30, 630, 381, 19));
        jedalenClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(jedalenClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 710, 31));
        jedalenClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(jedalenClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        jedalenClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(jedalenClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        jedalenClass->setStatusBar(statusBar);

        retranslateUi(jedalenClass);
        QObject::connect(pushButton, SIGNAL(clicked()), jedalenClass, SLOT(prihl()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), jedalenClass, SLOT(admn()));
        QObject::connect(label_2, SIGNAL(linkActivated(QString)), jedalenClass, SLOT(prihl()));
        QObject::connect(label_4, SIGNAL(linkActivated(QString)), jedalenClass, SLOT(prihl()));
        QObject::connect(comboBox, SIGNAL(activated(QString)), jedalenClass, SLOT(dni_menu()));
        QObject::connect(label_5, SIGNAL(linkActivated(QString)), jedalenClass, SLOT(dni_menu()));
        QObject::connect(label_6, SIGNAL(linkActivated(QString)), jedalenClass, SLOT(dni_menu()));
        QObject::connect(label_7, SIGNAL(linkActivated(QString)), jedalenClass, SLOT(dni_menu()));
        QObject::connect(label_8, SIGNAL(linkActivated(QString)), jedalenClass, SLOT(dni_menu()));
        QObject::connect(label_9, SIGNAL(linkActivated(QString)), jedalenClass, SLOT(dni_menu()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), jedalenClass, SLOT(odhl()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), jedalenClass, SLOT(vyber()));
        QObject::connect(comboBox, SIGNAL(activated(QString)), jedalenClass, SLOT(vyber()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), jedalenClass, SLOT(dobit()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), jedalenClass, SLOT(vymazat()));

        QMetaObject::connectSlotsByName(jedalenClass);
    } // setupUi

    void retranslateUi(QMainWindow *jedalenClass)
    {
        jedalenClass->setWindowTitle(QApplication::translate("jedalenClass", "jedalen", 0));
        label->setText(QApplication::translate("jedalenClass", "Jed\303\241le\305\210", 0));
        pushButton->setText(QApplication::translate("jedalenClass", "Prihl\303\241senie", 0));
        pushButton_2->setText(QApplication::translate("jedalenClass", "Admin", 0));
        label_2->setText(QApplication::translate("jedalenClass", "U\305\276\303\255vate\304\276", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("jedalenClass", "Dni", 0)
         << QApplication::translate("jedalenClass", "Pondelok", 0)
         << QApplication::translate("jedalenClass", "Utorok", 0)
         << QApplication::translate("jedalenClass", "Streda", 0)
         << QApplication::translate("jedalenClass", "\305\240tvrtok", 0)
         << QApplication::translate("jedalenClass", "Piatok", 0)
        );
        label_3->setText(QApplication::translate("jedalenClass", "Kredit:", 0));
        label_4->setText(QApplication::translate("jedalenClass", "Pohlavie", 0));
        checkBox->setText(QString());
        checkBox_2->setText(QString());
        checkBox_3->setText(QString());
        checkBox_4->setText(QString());
        checkBox_5->setText(QString());
        label_5->setText(QApplication::translate("jedalenClass", "0,0", 0));
        label_6->setText(QApplication::translate("jedalenClass", "0,0", 0));
        label_7->setText(QApplication::translate("jedalenClass", "0,0", 0));
        label_8->setText(QApplication::translate("jedalenClass", "0,0", 0));
        label_9->setText(QApplication::translate("jedalenClass", "0,0", 0));
        pushButton_3->setText(QApplication::translate("jedalenClass", "Odhl\303\241senie", 0));
        pushButton_4->setText(QApplication::translate("jedalenClass", "Objednaj", 0));
        label_10->setText(QApplication::translate("jedalenClass", "Objednan\303\251", 0));
        label_11->setText(QApplication::translate("jedalenClass", "Cena", 0));
        pushButton_5->setText(QApplication::translate("jedalenClass", "Vymaza\305\245", 0));
        pushButton_6->setText(QApplication::translate("jedalenClass", "Dobi\305\245 kredit", 0));
        label_12->setText(QApplication::translate("jedalenClass", "Zadajte sumu, o ktoru chcete zvysit svoj kredit.", 0));
    } // retranslateUi

};

namespace Ui {
    class jedalenClass: public Ui_jedalenClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JEDALEN_H
