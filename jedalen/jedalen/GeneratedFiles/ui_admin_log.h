/********************************************************************************
** Form generated from reading UI file 'admin_log.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMIN_LOG_H
#define UI_ADMIN_LOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_admin_log
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton;

    void setupUi(QDialog *admin_log)
    {
        if (admin_log->objectName().isEmpty())
            admin_log->setObjectName(QStringLiteral("admin_log"));
        admin_log->resize(400, 300);
        label = new QLabel(admin_log);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 60, 68, 19));
        label_2 = new QLabel(admin_log);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(60, 100, 68, 19));
        lineEdit = new QLineEdit(admin_log);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(130, 60, 113, 25));
        lineEdit_2 = new QLineEdit(admin_log);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(130, 100, 113, 25));
        lineEdit_2->setInputMethodHints(Qt::ImhHiddenText|Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText|Qt::ImhSensitiveData);
        lineEdit_2->setEchoMode(QLineEdit::Password);
        pushButton = new QPushButton(admin_log);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(130, 170, 112, 34));

        retranslateUi(admin_log);
        QObject::connect(pushButton, SIGNAL(clicked()), admin_log, SLOT(click()));

        QMetaObject::connectSlotsByName(admin_log);
    } // setupUi

    void retranslateUi(QDialog *admin_log)
    {
        admin_log->setWindowTitle(QApplication::translate("admin_log", "admin_log", 0));
        label->setText(QApplication::translate("admin_log", "Meno", 0));
        label_2->setText(QApplication::translate("admin_log", "Heslo", 0));
        lineEdit_2->setText(QString());
        pushButton->setText(QApplication::translate("admin_log", "Prihl\303\241senie", 0));
    } // retranslateUi

};

namespace Ui {
    class admin_log: public Ui_admin_log {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMIN_LOG_H
