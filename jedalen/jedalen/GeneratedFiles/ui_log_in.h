/********************************************************************************
** Form generated from reading UI file 'log_in.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOG_IN_H
#define UI_LOG_IN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_log_in
{
public:
    QLabel *label;
    QPushButton *pushButton;
    QComboBox *comboBox;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton_2;

    void setupUi(QDialog *log_in)
    {
        if (log_in->objectName().isEmpty())
            log_in->setObjectName(QStringLiteral("log_in"));
        log_in->resize(513, 477);
        label = new QLabel(log_in);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 70, 221, 19));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        pushButton = new QPushButton(log_in);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(200, 200, 112, 34));
        comboBox = new QComboBox(log_in);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(30, 120, 191, 25));
        label_2 = new QLabel(log_in);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(360, 70, 68, 19));
        label_2->setFont(font);
        lineEdit = new QLineEdit(log_in);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(320, 120, 141, 25));
        lineEdit->setInputMethodHints(Qt::ImhHiddenText|Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText|Qt::ImhSensitiveData);
        lineEdit->setEchoMode(QLineEdit::Password);
        pushButton_2 = new QPushButton(log_in);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(30, 200, 112, 34));

        retranslateUi(log_in);
        QObject::connect(pushButton, SIGNAL(clicked()), log_in, SLOT(prihl_click()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), log_in, SLOT(nacitaj()));

        QMetaObject::connectSlotsByName(log_in);
    } // setupUi

    void retranslateUi(QDialog *log_in)
    {
        log_in->setWindowTitle(QApplication::translate("log_in", "log_in", 0));
        label->setText(QApplication::translate("log_in", "Prihlasovacie meno", 0));
        pushButton->setText(QApplication::translate("log_in", "Prihl\303\241si\305\245", 0));
        label_2->setText(QApplication::translate("log_in", "Heslo", 0));
        lineEdit->setText(QString());
        pushButton_2->setText(QApplication::translate("log_in", "nacitaj", 0));
    } // retranslateUi

};

namespace Ui {
    class log_in: public Ui_log_in {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOG_IN_H
