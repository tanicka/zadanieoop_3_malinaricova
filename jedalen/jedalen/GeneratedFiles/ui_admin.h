/********************************************************************************
** Form generated from reading UI file 'admin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMIN_H
#define UI_ADMIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_admin
{
public:
    QPushButton *pushButton;
    QComboBox *comboBox;
    QLabel *label;
    QPushButton *pushButton_2;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QLabel *label_4;
    QLineEdit *lineEdit_2;
    QLabel *label_5;
    QLineEdit *lineEdit_3;
    QPushButton *pushButton_3;
    QLabel *label_6;
    QLineEdit *lineEdit_4;

    void setupUi(QDialog *admin)
    {
        if (admin->objectName().isEmpty())
            admin->setObjectName(QStringLiteral("admin"));
        admin->resize(564, 483);
        pushButton = new QPushButton(admin);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(30, 20, 112, 34));
        comboBox = new QComboBox(admin);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(30, 70, 171, 25));
        label = new QLabel(admin);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(190, 30, 381, 19));
        pushButton_2 = new QPushButton(admin);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(240, 70, 112, 34));
        label_2 = new QLabel(admin);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 160, 351, 19));
        label_3 = new QLabel(admin);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 200, 131, 19));
        lineEdit = new QLineEdit(admin);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(180, 200, 201, 25));
        label_4 = new QLabel(admin);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(30, 280, 68, 19));
        lineEdit_2 = new QLineEdit(admin);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(180, 240, 201, 25));
        label_5 = new QLabel(admin);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(30, 240, 68, 19));
        lineEdit_3 = new QLineEdit(admin);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(180, 280, 201, 25));
        pushButton_3 = new QPushButton(admin);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(140, 380, 112, 34));
        label_6 = new QLabel(admin);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(30, 320, 68, 19));
        lineEdit_4 = new QLineEdit(admin);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(180, 320, 201, 25));

        retranslateUi(admin);
        QObject::connect(pushButton, SIGNAL(clicked()), admin, SLOT(nac_pouz()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), admin, SLOT(zapis()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), admin, SLOT(zmazat()));

        QMetaObject::connectSlotsByName(admin);
    } // setupUi

    void retranslateUi(QDialog *admin)
    {
        admin->setWindowTitle(QApplication::translate("admin", "admin", 0));
        pushButton->setText(QApplication::translate("admin", "Pou\305\276\303\255vatelia", 0));
        label->setText(QApplication::translate("admin", "Vyberte pou\305\276\303\255vate\304\276a, ktor\303\251ho chcete odstr\303\241ni\305\245", 0));
        pushButton_2->setText(QApplication::translate("admin", "Vymaza\305\245", 0));
        label_2->setText(QApplication::translate("admin", "Zap\303\255\305\241te nov\303\251ho pou\305\276\303\255vate\304\276a", 0));
        label_3->setText(QApplication::translate("admin", "Meno a priezvisko", 0));
        label_4->setText(QApplication::translate("admin", "Heslo", 0));
        label_5->setText(QApplication::translate("admin", "Pohlavie", 0));
        pushButton_3->setText(QApplication::translate("admin", "Prida\305\245", 0));
        label_6->setText(QApplication::translate("admin", "Kredit", 0));
    } // retranslateUi

};

namespace Ui {
    class admin: public Ui_admin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMIN_H
