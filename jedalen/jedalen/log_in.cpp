﻿#include "log_in.h"
#include<QFile>


log_in::log_in(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);\
	
}

log_in::~log_in() {
	
}

void log_in::nacitaj() {
	QString line;
	QFile subor_pouz("pouzivatelia.txt");

	if (subor_pouz.open(QFile::ReadOnly)) {
		QTextStream pouzivatel(&subor_pouz);
		while (!pouzivatel.atEnd()) {
			line = pouzivatel.readLine();
			ui.comboBox->addItem(line);
		}
		subor_pouz.close();
	}
}

int log_in::dajMeno()
{
	meno = ui.comboBox->currentIndex();
	return meno;
}

QString log_in::dajMeno2()
{
	meno2 = ui.comboBox->currentText();
	return meno2;
}

QString log_in::dajHeslo()
{
	return heslo;
}

void log_in::prihl_click()
{
	meno2 = ui.comboBox->currentText();
	heslo = ui.lineEdit->text();
	close();
}

